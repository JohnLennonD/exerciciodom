let btnEnviar = document.querySelector('#btnEnviar')
btnEnviar.addEventListener('click', createMessage)



function createMessage() {
    let message = document.querySelector('#txtArea').value
    if (message == '') {
        return
    }
    let boxMessage = document.querySelector('.showMessages')
    boxMessage.innerHTML += `
        <div class="messageArea">
            <div class="message" id="">
                <textarea class='txtMessage' disabled>${message}</textarea>
            </div>
            
            <button class="btnEdit" onclick="editMessage(this)">Editar</button>
            <button class="btnDel" onclick="deleteMessage(this)">Excluir</button>
        </div>`
    
    document.querySelector('#txtArea').value = ''
}

function deleteMessage(e) {
    let containerAreaMessage = e.parentElement;
    containerAreaMessage.remove()
}

function editMessage (e) {
    // let message = document.querySelector('#txtArea').value

    let boxMessageArea = e.parentNode;
    let boxTxtArea = boxMessageArea.querySelector('.txtMessage')
    console.log(boxTxtArea)
    // selecting the opposite of the current
    boxTxtArea.disabled = !boxTxtArea.disabled
}